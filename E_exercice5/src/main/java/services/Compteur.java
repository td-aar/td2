package services;

import org.springframework.stereotype.Service;

@Service
public class Compteur {
    private Integer value = 0;

    public void increment() {
        this.value+=1;
    }

    public Integer getValue() {
        return this.value;
    }
}
