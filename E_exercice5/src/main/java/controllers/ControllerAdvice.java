package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import services.Compteur;
import services.Facade;

@SessionAttributes("compteur")
@org.springframework.web.bind.annotation.ControllerAdvice
public class ControllerAdvice {
    @Autowired
    private Facade facade;

    @ModelAttribute
    public void addGlobalAttribute(Model model) {
        getFacade().incremente();
        model.addAttribute("compteur", getFacade().getValue());
    }

    public Facade getFacade() {
        return facade;
    }
}
