package config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import services.Compteur;
import services.Facade;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class BeanConfig {
    @Bean
    public Facade facade() {
        return new Facade();
    }

    @Bean
    public Compteur compteur() {
        return new Compteur();
    }
}
