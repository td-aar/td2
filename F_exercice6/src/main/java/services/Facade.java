package services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

public class Facade {
    private Map<String,String> users;
    @Autowired
    private Compteur compteur;

    @PostConstruct
    public void init(){
        users=new HashMap<>();
        users.put("alice","alice");
        users.put("bob","bob");
    }

    public boolean checkLP(String login,String password) {
        String pwd=users.get(login);
        return ((pwd!=null) && (pwd.equals(password)));
    }

    public void incremente() {
        this.compteur.increment();
    }

    public Integer getValue() {
        return this.compteur.getValue();
    }

}
