package dtos;

public enum Humeur {
    FINE,
    ANGRY,
    SAD,
    HAPPY
}
