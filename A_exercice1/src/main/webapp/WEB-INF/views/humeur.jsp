<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Humeur</title>
</head>
<body>
<h1>HUMEUR</h1>

<form method="post" action="${pageContext.request.contextPath}/humeur">

    <select name="humDTO">
        <c:forEach items="${humeurs}" var="humeur">
            <option value="${humeur}">${humeur}</option>
        </c:forEach>
    </select>
    <p><button type="submit">Envoyer</button></p>
</form>


<c:import url="session.jsp"></c:import>

</body>
</html>
