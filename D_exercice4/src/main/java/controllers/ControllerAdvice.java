package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import services.Compteur;

@SessionAttributes("compteur")
@org.springframework.web.bind.annotation.ControllerAdvice
public class ControllerAdvice {
    @Autowired
    private Compteur compteur;

    @ModelAttribute
    public void addGlobalAttribute(Model model) {
        getCompteur().increment();
        model.addAttribute("compteur", getCompteur().getValue());
    }

    public Compteur getCompteur() {
        return compteur;
    }
}
